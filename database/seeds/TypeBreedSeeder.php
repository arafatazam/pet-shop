<?php

use Illuminate\Database\Seeder;
use App\PetType;

class TypeBreedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $types=[
            'Cat'=> ['Persian', 'Ragdoll', 'Abyssinian'],
            'Dog'=> ['Siberian Husky', 'Pug', 'Beagle'],
            'Bird'=> ['Cockatiels', 'Macaws'],
            'Reptile'=> ['Anatolian Viper', 'Banded Day Gecko'],
        ];

        DB::table('pet_types')->truncate();
        DB::table('breeds')->truncate();

        foreach ($types as $type=>$breeds){
            $pt = new PetType();
            $pt->title = $type;
            $pt->save();
            foreach ($breeds as $breed){
                $pt->breeds()->create(['title'=>$breed]);
            }
        }
    }
}
