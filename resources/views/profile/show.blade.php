@extends('templates.main')

@section('content')

    <div class="login main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 overflow login-container-wrapper clear-padding">
                    <div class="login-container">
                        <div class="stage stage-2">
                            <form method="post" action="#">
                                <div class="row m-b-30">
                                    <label style="width:49%">Name:</label>
                                    <label style="width:49%">Email:</label>
                                    <input type="text" placeholder="Name" name="name" value="{{$my['name']}}" disabled>
                                    <input type="email" placeholder="Email Address" name="email" value="{{$my['email']}}" disabled>
                                    <label>Phone Number:</label>
                                    <input type="text" style="width: 100%" value="{{$my['phone_number']}}" placeholder="Phone Number" name="phone_number" disabled>
                                    <label>Address:</label>
                                    <input type="text" style="width: 100%" value="{{$my['address']}}" placeholder="Address" name="address" disabled>
                                    <label>Adoptions:</label>
                                    <textarea placeholder="Information regarding adoption (ie: time, holidays etc)" name="adoptions" disabled>{{$my['adoptions']}}</textarea>
                                    <a style="text-align: center" class="white basic-bg-color login-btn bold" href="{{route('profile.edit')}}">EDIT PROFILE</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection