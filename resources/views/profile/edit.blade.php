@extends('templates.main')

@section('content')

    <div class="login main">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 overflow login-container-wrapper clear-padding">
                    <div class="login-container">
                        <div class="stage stage-2">
                            <form method="post" action="{{route('profile.update')}}">
                                {{csrf_field()}}
                                <div class="row m-b-30">
                                    <label style="width:49%">Name:</label>
                                    <label style="width:49%">Email:</label>
                                    <input type="text" placeholder="Name" name="name" value="{{$my['name']}}" required>
                                    <input type="email" placeholder="Email Address" name="email" value="{{$my['email']}}" disabled>
                                    @if($errors->regErrors->has('name'))
                                        <span class="text-danger">{{$errors->regErrors->first('name')}}</span>
                                    @endif
                                    <label>Phone Number:</label>
                                    <input type="text" style="width: 100%" value="{{$my['phone_number']}}" placeholder="Phone Number" name="phone_number" required>
                                    @if($errors->regErrors->has('phone_number'))
                                        <span class="text-danger">{{$errors->regErrors->first('phone_number')}}</span>
                                    @endif
                                    <label>Address:</label>
                                    <input type="text" style="width: 100%" value="{{$my['address']}}" placeholder="Address" name="address" required>
                                    @if($errors->regErrors->has('address'))
                                        <span class="text-danger">{{$errors->regErrors->first('address')}}</span>
                                    @endif
                                    <label>Adoptions:</label>
                                    <textarea placeholder="Information regarding adoption (ie: time, holidays etc)" name="adoptions" required>{{$my['adoptions']}}</textarea>
                                    @if($errors->regErrors->has('adoptions'))
                                        <span class="text-danger">{{$errors->regErrors->first('adoptions')}}</span>
                                    @endif
                                    <label>Current Password:</label>
                                    <input type="password" placeholder="Password" name="password" required>
                                    @if($errors->regErrors->has('password'))
                                        <span class="text-danger">{{$errors->regErrors->first('password')}}</span>
                                    @endif
                                    <button type="submit" class="white basic-bg-color login-btn bold">UPDATE PROFILE</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection