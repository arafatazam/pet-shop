@extends('templates.main')

@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/blueimp-gallery.min.css')}}" media="all" />
@endpush


@section('content')


    @include('commons._search')


    <!--============================
	Start : Animal details
	=============================-->


    <div class="animalDetails p-b-50 p-t-60" id="profile">
        <div class="container">
            <div class="row" id="pet-profile-page">
                <div class="col-sm-1 less-padding text-center hidden-xs">
                    <!--social icon-->
                    <div class="col-xs-12">
                        <a target="_blank" href="#" class="btn btn-default col-xs-12 share fb clear-margin-top"><i class="fa fa-facebook fa-lg"></i></a>
                    </div>

                    <div class="col-xs-12">
                        <a target="_blank" href="#" class="btn btn-default col-xs-12 share tweet">
                            <i class="fa fa-twitter fa-lg"></i>
                        </a>
                    </div>

                    <div class="col-xs-12">
                        <a href="#" class="btn btn-info col-xs-12 share donate" target="_blank"><i class="fa fa-google-plus fa-lg"></i></a>
                    </div>
                </div>
                <div class="col-sm-3">

                    <div class="row">
                        <div class="row">
                            <div class="col-xs-12">
                                <p class="basic-color _20px bold s-text-center">{{strtoupper($pet->name)}}</p>
                            </div>
                        </div>

                        <div class="col-xs-10 col-xs-offset-1 col-sm-12 col-sm-offset-0 overflow clear-padding slide-container">
                            <div id="prev">
                                <img src="{{asset('assets/images/icon/prev.png')}}" alt="" class="img-responsive">
                            </div>

                            <div id="next">
                                <img src="{{asset('assets/images/icon/next.png')}}" alt="" class="img-responsive">
                            </div>

                            <!--slider option-->
                            <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>

                            <div class="slider-wrapper" id="links">
                                <!--slide 1-->
                                <div class="slide slide-1">
                                    <a href="{{$pet['image1']}}">
                                        <img src="{{$pet['image1']}}" alt="" class="img-responsive relative center-x">
                                    </a>

                                </div>


                            @if(isset($pet['image2']))
                                <!--slide 2-->
                                <div class="slide slide-2">
                                    <a href="{{$pet['image2']}}">
                                        <img src="{{$pet['image2']}}" alt="" class="img-responsive relative center-x">
                                    </a>
                                </div>
                            @endif

                            @if(isset($pet['image3']))
                                <!--slide 3-->
                                <div class="slide slide-3">
                                    <a href="{{$pet['image3']}}">
                                        <img src="{{$pet['image3']}}" alt="" class="img-responsive relative center-x">
                                    </a>
                                </div>
                            @endif

                            </div>

                        </div>
                    </div>




                    <div class="row">
                        <div class="col-sm-4 hidden-xs less-padding">
                            <img src="{{$pet['image1']}}" alt="" class="thumbnail opacity img-responsive" data-index=0>
                        </div>

                        @if(isset($pet['image2']))
                        <div class="col-sm-4 hidden-xs less-padding">
                            <img src="{{$pet['image2']}}" alt="" class="thumbnail img-responsive" data-index=1>
                        </div>
                        @endif
                        @if(isset($pet['image3']))
                        <div class="col-sm-4 hidden-xs less-padding">
                            <img src="{{$pet['image3']}}" alt="" class="thumbnail img-responsive" data-index=2>
                        </div>
                        @endif
                        <!-- if you want to add more thumbnail, then copy and the mark up and change the 'data-index' number-->
                    </div>







                    <div class="row light-blue-bkg visible-xs" id="notification">
                        <div class="col-xs-12">
                            <p>Dear Visitor, please note a property check may be required before adoption to ensure the safety and welfare of our animals. Rental agreements allowing pets will be required for adoptions into rental properties.</p>
                            <p>Please note that this website is live and updates frequently, there is a chance that by the time you arrive at our location the animal you have an interest in might have been adopted by another party. We unfortunately can not put animals on hold over the phone.</p>
                        </div>
                    </div>





                </div>



                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-6">

                            <div class="col-xs-12 green-bkg" id="pet-id">
                                <h3><b>Animal ID:</b> {{$pet['id']}}</h3>
                            </div>
                            <div class="col-xs-12" id="pet-info">

                                <p class="category"><b>Name:</b> {{$pet['name']}}</p>
                                <p class="category"><b>Type:</b> {{$pet->type->title or 'Unknown'}}</p>
                                <p class="category"><b>Breed:</b> {{$pet->breed->title or 'Unknown'}} </p>
                                <p class="category"><b>Sex:</b> {{$pet['sex'] or 'Unknown'}} </p>
                                <p class="category"><b>Size:</b> {{$pet['size'] or 'Unknown'}} </p>
                                <p class="category"><b>Colour:</b> {{$pet['color'] or 'Unknown'}}</p>
                                <p class="category"><b>Age:</b> {{$pet['age'] or 'Unknown'}}</p>
                            </div>



                            <div class="col-xs-12 light-blue-bkg" id="notification">
                                <p>Dear Visitor, please note a property check may be required before adoption to ensure the safety and welfare of our animals. Rental agreements allowing pets will be required for adoptions into rental properties.</p>
                                <p>Please note that this website is live and updates frequently, there is a chance that by the time you arrive at our location the animal you have an interest in might have been adopted by another party. We unfortunately can not put animals on hold over the phone.</p>
                            </div>




                        </div>

                        <div class="col-sm-6">
                            <div class="col-xs-12 light-green-bkg" id="contact-pet">
                                <h3 class="green">Where to find me</h3>
                                <p class="category"><b>Location:</b> {{$pet->location_string}} </p>
                                <p class="category"><b>Phone:</b> <a class="shelter_phone" href="tel:{{$pet['user']['phone_number']}}"> {{$pet['user']['phone_number']}}</a></p>
                                <p class="category"><b>Address:</b></p>
                                <p>{{$pet['user']['address']}}</p>
                                <p class="category"><b>Adoptions:</b></p>
                                <p>{!! nl2br($pet['user']['adoptions']) !!}</p>
                            </div>

                            <div class="col-xs-12 less-padding m-t-30">
                                <form action="" method="POST">
                                    <textarea name="comment" class="textarea" placeholder="YOUR MASSAGE"></textarea>
                                    <button type="submit" class="comment-btn white bold _18px">SEND</button>
                                </form>
                            </div>
                        </div>

                    </div>

                </div>
                <div class="col-xs-12 side-bar">
                    <div class="row visible-xs" id="recently-viewed">

                        <div class="col-xs-12 collapse" id="recent-pets-mobile">
                            <div class="row">
                                <a href="#" class="nounderline">
                                    <div class="col-xs-12 col-lg-3 pets-picture" style="background-image: url(../../assets/images/animal/cock.png);"></div>
                                    <div class="col-xs-12 col-lg-7 pets-name-profile-page">
                                        <h4 class="green"> Tulsa</h4>
                                        <p>Silkie</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="row visible-xs text-center">
                        <div class="col-xs-12"><a target="_blank" href="#" class="btn btn-default col-xs-12 share fb"><i class="fa fa-facebook fa-lg"></i></a></div>

                        <div class="col-xs-12"><a target="_blank" href="#" class="btn btn-default col-xs-12 share tweet"><i class="fa fa-twitter fa-lg"></i></a></div>
                        <div class="col-xs-12"><a href="#donate-now" class="btn btn-info col-xs-12 share donate">Donate Now</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--============================
    End : Animal details
    =============================-->
    @include('pet._similar')

@endsection

@push('scripts')
<script src="{{asset('assets/js/plugins/blueimp-gallery.min.js')}}" type="text/javascript"></script><!--for pop up-->
<script>
    document.getElementById('links').onclick = function (event) {
        event = event || window.event;
        var target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {index: link, event: event},
            links = this.getElementsByTagName('a');
        blueimp.Gallery(links, options);
    };
</script>
@include('commons._type-loading-script')
@endpush