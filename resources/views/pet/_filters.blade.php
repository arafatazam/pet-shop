<div class="col-xs-12 col-sm-3 different-bg-color">
    <p class="_24px black s-text-center m-t-15">REFINE SEARCH</p>
    <form action="{{route('search',request()->all())}}" method="get">
        <p>Pet Type</p>
        <select name="type_id" class="select m-b-15 type-selection">
        </select>

        <p>Pet Breed</p>
        <select name="breed_id" class="select m-b-15 breed-selection">
        </select>

        {{--<p>Advert Type</p>--}}
        {{--<select name="" class="select m-b-15">--}}
            {{--<option value="">All advert type</option>--}}
            {{--<option value="">All advert type</option>--}}
            {{--<option value="">All advert type</option>--}}
            {{--<option value="">All advert type</option>--}}
        {{--</select>--}}

        <p>Location</p>
        <input type="text" name="location_str" id="location-autocomplete" class="text m-b-15" placeholder="Town of Postcord" value="{{ request()->get('location_str')}}">
        <input name="coordinates" id="geolocation-input" type="hidden" value="{{ request()->get('coordinates')}}">

        <p>Distance</p>
        <input type="range" name="max_distance" class="input-range-1" min="0" max="1000" step="10" value="0">
        <p class="range-value"></p>

        <p>Price</p>
        <input type="range" name="max_price" class="input-range-2" min="0" max="1000" step="10" value="0">
        <p class="range-value"></p>


        {{--<p>Key Word</p>--}}
        {{--<input type="text" class="text m-b-15" >--}}

        {{--<p>Result Per Page</p>--}}
        {{--<select name="" class="select m-b-15">--}}
            {{--<option value="">All advert type</option>--}}
            {{--<option value="">All advert type</option>--}}
            {{--<option value="">All advert type</option>--}}
            {{--<option value="">All advert type</option>--}}
        {{--</select>--}}

        <button type="submit" class="white bold basic-bg-color up-btn">UPDATE SEARCH</button>
    </form>
</div>
<!--Right Sight Box-->