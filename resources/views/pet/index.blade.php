@extends('templates.main')

@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/range.css')}}" media="all" />
@endpush

@section('content')

    @include('commons._search')

    <!--===================
	Start : Listing
	=====================-->

    <div class="listing p-b-10 p-t-10">
        <div class="container">
            <div class="row">

                @include('pet._filters')

                <div class="col-xs-12 col-sm-9">
                    <div class="row m-b-15 m-t-15">
                        <div class="col-xs-12 col-sm-9">
                            <p class="bold black _24px">PETS Found{{$location? " near $location":""}}</p>
                            <p>101 Dogs found in sydney</p>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <p >SORT BY :</p>
                            <form action="#" method="POST">
                                <select name="sortBy" id="sortBy">
                                    <option value="">Date update(Newest)</option>
                                    <option value="">Date update(Newest)</option>
                                    <option value="">Date update(Newest)</option>
                                    <option value="">Date update(Newest)</option>
                                </select>
                            </form>
                        </div>

                    </div>
                    @foreach($pets as $pet)
                    <!--row-->
                    <div class="row whole-wrapper m-b-15">
                        <div class="col-xs-10 col-xs-offset-1 col-sm-3 col-sm-offset-0">
                            <div class="pet-image-wrapper">
                                <a href="{{route('pet.show',[$pet->id])}}"><img src="{{$pet->image1}}" alt="" class="img-responsive"></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-7">
                            <div class="blog-wrapper">
                                <p class="_30px bold black f-r">{{$pet->price ? '$'.($pet->price+0) : ''}}</p>
                                <p class="bold _24px basic-color"><a class="basic-color" href="{{route('pet.show',[$pet->id])}}">{{$pet->name}}</a></p>
                                @if($pet->location_string)
                                <p class="black regular"><i class="fa fa-map-marker m-r-10"></i><span>{{$pet->location_string}}</span></p>
                                @endif
                                <p>{!! nl2br($pet->description) !!}</p>

                                <p class="m-t-30"><i class="fa fa-clock-o m-r-10" aria-hidden="true"></i><span>{{$pet->created_at->diffForHumans()}}</span></p>
                            </div>
                        </div>

                    </div>
                    <!--/row-->
                    @endforeach

                </div>



                <div class="col-xs-12 col-sm-3">
                    <div class="row m-b-15">
                        <div class="col-sm-12 visible-xs">
                            <div class="wrapper-inner">
                                <p class="_24px basic-color"> SORT BY :</p>

                                <ul class="list-unstyled black _20px">
                                    <li><a href="#">Newly Listed</a></li>
                                    <li><a href="#">Lower Price</a></li>
                                    <li><a href="#">First Higher</a></li>
                                    <li><a href="#">Price First</a></li>

                                </ul>
                            </div>

                        </div>
                    </div>

                    <div class="row m-b-15">
                        <div class="col-sm-12 visible-xs">
                            <div class="wrapper-inner">
                                <p class="_24px basic-color"> Refine by Price</p>

                                <ul class="list-unstyled black _20px">
                                    <li><a href="#">All categories</a></li>
                                    <li><a href="#">Fish</a></li>
                                    <li><a href="#">Cats</a></li>
                                    <li><a href="#">Dogs</a></li>
                                    <li><a href="#">Birds</a></li>
                                    <li><a href="#">Horses</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>


                    <div class="row">
                        <div class="col-sm-12 visible-xs">
                            <div class="wrapper-inner">
                                <p class="_24px basic-color"> Your Cheappest Pet Supplies Store</p>

                                <div class="row">
                                    <div class="col-xs-10 col-xs-offset-1">
                                        <img src="{{assert('assets/images/dog/ecommerce.png')}}" alt="" class="img-responsive">
                                    </div>
                                </div>
                                <div class="row">
                                    <a href="" id="post" class="_20px basic-bg-color text-uppercase text-center">CLICK HERE</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


    <!--===================
    End : Listing
    =====================-->

@endsection

@push('scripts')
<script src="{{asset('assets/js/plugins/jquery.zoom.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/plugins/mag.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/plugins/range.js')}}" type="text/javascript"></script>
@include('commons._type-loading-script')
@include('commons._gmap-location-scripts')
@endpush