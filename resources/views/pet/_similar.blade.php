<!--=====================

Start : Similar pets

=======================-->



<div class="property p-b-50 p-t-60">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center m-b-50 s-m-b-15">
                <p class="bold _30px basic-color">SIMILAR PETS</p>
            </div>
            <div class="col-xs-12">
                <div class="property-item-container">
                    <div class="belt">
                        <div class="state state-1"><!--'.state-1'-->
                            @foreach($similar as $pet)
                            <!--1-->
                            <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0 col-md-3 less-padding-2 s-m-b-15 m-b-5">
                                <div class="inner-box">
                                    <div class="img-container">
                                        <img src="{{$pet->image1}}" alt="" class="img-responsive">
                                    </div>
                                    <a href="{{route('pet.show', [$pet->id] )}}">
                                    <div class="search-icon text">
                                        <p class="white-10">{{$pet->description}}</p>
                                    </div>
                                    </a>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <p class="basic-color bold">{{$pet->price ? '$'.($pet->price+0) : ''}}</p>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div><!--'.state-1'-->

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--===================

End:SIMILAR PETS

======================-->