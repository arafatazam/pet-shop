@extends('templates.main')

@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/fileinput.min.css')}}" media="all" />
@endpush


@section('content')
    <form method="post" action="{{route('pet.store')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <!--=================================
        image uplode
        ===================================-->
        <div class="main-container">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 page-content">
                        <div class="inner-box category-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-horizontal">
                                        <!--picture-->
                                        <div class="form-group">

                                            <div class="col-xs-12">
                                                <!--picture 1-->
                                                <div class="m-b-10">
                                                    <input name="image_file_1" id="input-upload-img1" type="file" class="file" data-preview-file-type="text" required>
                                                    @if($errors->has('image_file_1'))
                                                        <span class="text-danger">{{$errors->first('image_file_1')}}</span>
                                                    @endif
                                                </div>

                                                <!--picture 2-->
                                                <div class="m-b-10">
                                                    <input name="image_file_2" id="input-upload-img2" type="file" class="file" data-preview-file-type="text">
                                                    @if($errors->has('image_file_2'))
                                                        <span class="text-danger">{{$errors->first('image_file_2')}}</span>
                                                    @endif
                                                </div>

                                                <!--picture 3-->
                                                <div class="m-b-10">
                                                    <input name="image_file_3" id="input-upload-img3" type="file" class="file" data-preview-file-type="text">
                                                    @if($errors->has('image_file_3'))
                                                        <span class="text-danger">{{$errors->first('image_file_3')}}</span>
                                                    @endif
                                                </div>

                                                <p class="help-block">Add up to 3 photos. Use a real image of your
                                                    product, not catalogs.</p>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="item-form p-b-50 p-t-60">
            <div class="container">
                <div class="row">
                    <p class="basic-color _24px text-center bold">SELECT THE CATAGORY</p>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                    <div class="form-group m-b-50">
                        <select name="animal_type" id="animal-select">
                            <option selected="selected" data-target="0"> SELECT HERE</option>
                            @foreach($pet_types as $i=>$type)
                            <option value="{{$type['id']}}" data-target="{{$i+1}}">{{strtoupper($type['title'])}}</option>
                            @endforeach
                        </select>

                        <input name="location_string" type="text" id="location-autocomplete" class="fill-form m-b-10" placeholder="Location">
                        <input name="location" id="geolocation-input" type="hidden">
                    </div>

                    @for($i=0,$l=sizeof($pet_types);$i<$l;$i++)
                        <div class="form-group same-all " data-index="{{$i+1}}">
                            <p class="black _18px m-b-30">FILL UP THE FORM FOR {{strtoupper($pet_types[$i]['title'])}}</p>
                            <input type="text" class="fill-form m-b-10" placeholder="Name" name="{{str_slug($pet_types[$i]['title'],'_')}}_name">
                            <select name="{{str_slug($pet_types[$i]['title'],'_')}}_breed_id" class="fill-form m-b-10" required>
                                @foreach($pet_types[$i]['breeds'] as $breed)
                                <option value="{{$breed['id']}}" >{{$breed['title']}}</option>
                                @endforeach
                            </select>
                            <input type="number" class="fill-form m-b-10" placeholder="Price" name="{{str_slug($pet_types[$i]['title'],'_')}}_price" step="any">
                            <textarea name="{{str_slug($pet_types[$i]['title'],'_')}}_description" placeholder="Description"></textarea>
                            <input type="text" class="fill-form m-b-10" placeholder="Age" name="{{str_slug($pet_types[$i]['title'],'_')}}_age">
                            <input type="text" class="fill-form m-b-10" placeholder="Sex" name="{{str_slug($pet_types[$i]['title'],'_')}}_sex">
                            <input type="text" class="fill-form m-b-10" placeholder="Colour" name="{{str_slug($pet_types[$i]['title'],'_')}}_color">
                            <button type="submit" class="basic-bg-color white fill-btn">SUBMIT</button>
                        </div>
                    @endfor
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
<script src="{{asset('assets/js/plugins/fileinput.min.js')}}" type="text/javascript"></script>
<script>
    // initialize with defaults
    $("#input-upload-img1").fileinput();
    $("#input-upload-img2").fileinput();
    $("#input-upload-img3").fileinput();
</script>
@include('commons._gmap-location-scripts')
@endpush