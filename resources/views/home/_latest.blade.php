<!--===================

Start: ANIMAL LISTING

======================-->
<div class="property p-b-50">
    <div class="container">
        <div class="row m-b-40">
            <p class="_24px bold text-uppercase text-center basic-color">Latest Pets</p>
        </div>
        <div class="row">


            <div class="col-xs-12">
                <div class="property-item-container">
                    <div class="belt">
                        <div class="state state-1"><!--'.state-1'-->
                            @foreach($latest as $pet)
                            <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0 col-md-3 less-padding s-m-b-15 m-b-5">
                                <div class="inner-box">
                                    <div class="img-container">
                                        <img src="{{$pet->image1}}" alt="PetImage" class="img-responsive">
                                    </div>
                                    <a href="{{route('pet.show',[$pet->id])}}">
                                    <div class="search-icon text">
                                        <p class="white-10">{{$pet->location_string}}</p>
                                        <p class="bold">{{$pet->price ? '$'.($pet->price+0) : ''}}</p>
                                    </div>
                                    </a>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 text-center">
                                        <p class="bold basic-color _20px clear-margin">{{($pet->type)? strtoupper($pet->type->title) : ''}}</p>
                                        <p>{{$pet->price ? '$'.($pet->price+0) : ''}}</p>
                                    </div>

                                </div>
                            </div>
                            @endforeach
                        </div><!--'.state-1'-->
                    </div>
                </div>
            </div>
        </div>
        <!--view all btn-->
        <div class="row">
            <div class="co-xs-12">
                <a class="f-r text-uppercase white basic-bg-color view-btn" href="{{route('pet.index')}}" style="border-radius:10px; padding: 6px 15px">View all</a>
            </div>
        </div><!--/row-->
    </div>
</div>

<!--===================

End:ANIMAL LISTING

======================-->