@extends('templates.main')

@push('styles')
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/owl.carousel.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/owl.theme.default.css')}}" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/animate.css')}}" media="all" />
<link rel="stylesheet" type="text/css" href="{{asset('assets/css/plugins/preloder.css')}}" media="all" />
@endpush

@section('content')

    @include('commons._search')

    <!--===================
	Start : Catagory
	======================-->


    <div class="catagory p-b-50 p-t-50">
        <div class="container">

            <div class="row">
                <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3 text-center">
                    <div class="col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 less-padding">
                        <img src="assets/images/catagory/1.png" alt="" class="relative center-x img-responsive m-b-15">
                        <p> BIRDS </p>
                    </div>
                    <div class="col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 less-padding">
                        <img src="assets/images/catagory/2.png" alt="" class="relative center-x  img-responsive m-b-15">
                        <p>REPTAILS</p>
                    </div>
                    <div class="col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 less-padding">
                        <img src="assets/images/catagory/1.png" alt="" class="relative center-x  img-responsive m-b-15">
                        <p>BIRDS</p>
                    </div>
                    <div class="col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 less-padding">
                        <img src="assets/images/catagory/2.png" alt="" class="relative center-x  img-responsive m-b-15">
                        <p>REPTAILS</p>
                    </div>
                    <div class="col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 less-padding">
                        <img src="assets/images/catagory/1.png" alt="" class="relative center-x  img-responsive m-b-15">
                        <p>BIRDS</p>
                    </div>
                    <div class="col-xs-4 col-xs-offset-4 col-sm-2 col-sm-offset-0 less-padding">
                        <img src="assets/images/catagory/2.png" alt="" class="relative center-x  img-responsive m-b-15">
                        <p>REPTAILS</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--===================
    End : Catagory
    ======================-->

    @if(!empty($latest))
        @include('home._latest')
    @endif

    <div class="blog different-bg-color p-b-50 p-t-50">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-7 s-m-b-15 less-padding-2">
                    <div class="wrapper-blog common-wrapper">
                        <p class="_24px black bold text-uppercase">Animal Direct is a Pets Marketplace.it is the best Pet Classifieds Site USA.</p>


                        <p class="text-capitalize text-justify">Allow you to Sell Your Pet Online for free, you will not pay any fees or commission to Animal Direct as well as you can Buy Pets Online for free also.We are created Animal Direct to be unique online marketplace to list your Pets For Sale or show your Pets For Adoption USA.When you are say to yourself,
                            I want to Sell My Pet, you have to reach Animal Direct and register or login to your accountand post new ad, you have to take care when you list your ad to mention full details to make your listing clear and upload pictures to make it unique ad attract buyers easily.</p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4  text-center less-padding-2">

                    <div class="sign-up-wrapper common-wrapper">
                        <p class="_24px black bold text-uppercase">SIGN UP TODAY. IT’S FREE!</p>
                        <p class="text-capitalize">Over 594 ads listed in Animal Direct | Pets for Sale and Adoption | Sell and Buy Pets and Animals.</p>
                        <a href="{{route('login')}}" class="sign-btn bold">GET START</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script src="http://gregfranko.com/jquery.selectBoxIt.js/js/jquery.selectBoxIt.min.js"></script>
@include('commons._type-loading-script')
@endpush