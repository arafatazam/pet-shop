@extends('templates.main')

@section('content')
    <div class="login main">
        <div class="container">
            {{--<div class="row m-b-60">--}}
                {{--<div class="col-xs-12 col-sm-6 col-sm-offset-3 clear-padding s-p-r-15 s-p-l-15">--}}
                    {{--<p class="basic-color bold _24px"> Account Type</p>--}}

                    {{--<select class="select select-1">--}}
                        {{--<option value="">Standard Accoun</option>--}}
                        {{--<option value="">Breeder/Frequent Account</option>--}}
                        {{--<option value="">Rescue Centre Account</option>--}}
                    {{--</select>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-sm-offset-3 overflow login-container-wrapper clear-padding">
                    <div class="login-container">


                        <div class="stage stage-1">
                            <div class="row m-b-30">
                                <p class="_24px regular basic-color text-uppercase">Sign in below</p>
                                <p class="black text-capitalize">Welcome back</p>
                            </div>
                            <form method="post" action="{{route('login')}}">
                            {{csrf_field()}}
                            <div class="row m-b-30">
                                <input type="email" name="email" value="{{old('email')}}" placeholder="Email Address" autofocus required>
                                @if($errors->has('email'))
                                    <span class="text-danger">{{$errors->first('email')}}</span>
                                @endif
                                <input type="password" name="password" placeholder="Password" required>
                                @if($errors->has('password'))
                                    <span class="text-danger">{{$errors->first('password')}}</span>
                                @endif
                                <button type="submit" class="white basic-bg-color login-btn bold">SIGN IN</button>
                            </div>
                            </form>
                            <div class="row">
                                <p class="basic-color _18px f-l">NOT A MEMBER?</p>
                                <p class="basic-color regular _18px f-r " id="go-next">SIGN UP <i class="fa fa-arrow-right"></i></p>
                            </div>
                        </div>


                        <div class="stage stage-2" id="register">
                            <div class="row m-b-30">
                                <p class="_24px regular basic-color text-uppercase">Sign up below</p>
                                <p class="black text-capitalize">It's free! No obligations.</p>
                            </div>
                            <form method="post" action="{{route('register')}}">
                            {{csrf_field()}}
                            <div class="row m-b-30">
                                <input type="text" placeholder="Name" name="name" value="{{old('name')}}" required>
                                <input type="email" placeholder="Email Address" name="email" value="{{old('email')}}" required>
                                @if($errors->regErrors->has('name'))
                                    <span class="text-danger">{{$errors->regErrors->first('name')}}</span>
                                @endif
                                @if($errors->regErrors->has('email'))
                                    <span class="text-danger">{{$errors->regErrors->first('email')}}</span>
                                @endif
                                <input type="text" style="width: 100%" value="{{old('phone_number')}}" placeholder="Phone Number" name="phone_number" required>
                                @if($errors->regErrors->has('phone_number'))
                                    <span class="text-danger">{{$errors->regErrors->first('phone_number')}}</span>
                                @endif
                                <input type="password" placeholder="Password" name="password" required>
                                @if($errors->regErrors->has('password'))
                                    <span class="text-danger">{{$errors->regErrors->first('password')}}</span>
                                @endif
                                <input type="password" placeholder="Repeat Password" name="password_confirmation" required>
                                <button type="submit" class="white basic-bg-color login-btn bold">SIGN UP</button>
                            </div>
                            </form>
                            <div class="row">
                                <p class="basic-color regular _18px" id="go-next" tilte="GO TO SIGN UP PAGE"><i class="fa fa-arrow-left"></i>SIGN IN</p>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('assets/js/plugins/jquery.zoom.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/js/plugins/mag.js')}}" type="text/javascript"></script>
@endpush