<!DOCTYPE HTML>
<html lang="en-US">
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en-US"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en-US"> <![endif]-->
<!--[if gt IE 8]><!-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="keywords" content="hand watch, hand watch in bangladesh" />
    <meta name="description" content="we are selling the best quality products and we export all over bangladesh.. " />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!--title of the site-->
    <title>PET | HOME</title>

    <!--bootstrap-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <!--google font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet">
    <!--icon-->
    <!--[note:we used font-awesome for font-icon]-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/fonts/font-awesome-4.6.3/css/font-awesome.min.css')}}">

    <!-- ========================
            Stylesheets
    =========================-->

    <!--plugings-->
    @stack('styles')

    <!--Custom css file-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}" media="all" />


    <!--====================
            Fav & icon
    ====================-->

    <link rel="shortcut icon" type="image/x-icon" href="https://www.adoptapet.com.au/icons/favicon.ico" />
    <link rel="apple-touch-icon" href="https://www.adoptapet.com.au/icons/favicon.ico">
    <link rel="apple-touch-icon" sizes="72x72" href="https://www.adoptapet.com.au/icons/favicon.icog">
    <link rel="apple-touch-icon" sizes="114x114" href="https://www.adoptapet.com.au/icons/favicon.ico">



    <!-- [if lt ie 9]> <script type="text/javascript" src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> <![endif] -->
</head>




<body class="light _16px"> <!--NOTE: body default font is 'lato', '16px' , 'light'-->

<div class="header"  data-stellar-background-ratio="0.8">
    @include('commons._main-nav')
</div>

@yield('content')

@include('commons._footer')



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('assets/js/plugins/jquery-2.2.3.min.js')}}" type="text/javascript"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('assets/js/plugins/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/plugins/jquery.nicescroll.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/plugins/wow.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/plugins/jquery.stellar.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/plugins/owl.carousel.min.js')}}" type="text/javascript"></script>

@stack('scripts')

<!--main js file-->
<script src="{{asset('assets/js/custom.js')}}" type="text/javascript"></script>


</body>
</html>