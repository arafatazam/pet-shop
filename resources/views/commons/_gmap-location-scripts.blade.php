<script async defer src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_GEO_KEY')}}&libraries=places&callback=initMap"></script>
<script type="text/javascript">
    function initMap() {
        var input = document.getElementById('location-autocomplete');
        var location = document.getElementById('geolocation-input');
        var autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.addListener('place_changed', function () {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            var coordinates = place.geometry.location.lng()+','+place.geometry.location.lat();
            location.value = coordinates;
        });
    }
</script>