<!--NAVIGATION-->
<nav class="navbar navbar-default fixed-navbar navigation sticky-navbar header-nav">
    <div class="container">
        <div class="navbar-header page-scroll">
            <a href="{{route('home')}}" class="navbar-brand brand-name">
                <img src="{{asset('assets/images/logo.png')}}" alt="logo" class="img-responsive brand-logo">
            </a>

            <button class="navbar-toggle nav-btn" data-toggle="collapse" data-target=".navHeaderCollapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <form id="logout" class="hidden" method="post" action="{{route('logout')}}">{{csrf_field()}}</form>

        <div class="collapse navbar-collapse navHeaderCollapse">
            <ul class="nav navbar-nav navbar-right lato light _18px header-main" id="menu">
                <li class="active "><a  href="{{route('home')}}">Home</a></li>
                @if(auth()->user())
                <li><a href="{{route('profile.show')}}">About Me</a></li>
                <li><a href="#" onclick="$('#logout').submit()">LOG OUT</a></li>
                <li><a href="{{route('pet.create')}}" id="post" class="basic-bg-color white">NEW POST</a></li>
                @else
                <li><a href="{{route('login')}}" id="login">LOG IN</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>