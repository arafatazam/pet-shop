<footer class="basic-bg-color main">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center white">
                <p>© 2016 Anima</p>
                <p>| Direct | Pets for Sale and Adoption |</p>
                <p>| Sell and Buy Pets and Animals. All Rights Reserved. |</p>
            </div>
        </div>
    </div>
</footer>