<script>
    $(document).ready(function () {

        var types = {!! App\PetType::with('breeds')->get()->keyBy('id')->toJson() !!};

        var typesOpt = '';

        for(var i in types){
            typesOpt+= '<option value="'+types[i].id+'">'+types[i].title+'</option>';
        }

        $('.type-selection').append(typesOpt);

        $('.type-selection').change(function (e) {

            $('.type-selection').val(this.value);

            if(!this.value){
                return;
            }

            var breeds = types[this.value]['breeds'];

            var breedsOpt = '<option value="">Animal Breed</option>';

            for(var i in breeds){
                breedsOpt+= '<option value="'+breeds[i].id+'">'+breeds[i].title+'</option>';
            }
            $('.breed-selection').html(breedsOpt);
        });

        $('.breed-selection').change(function (e) {
            $('.breed-selection').val(this.value);
        });

        $('.sex-selection').change(function (e) {
            $('.sex-selection').val(this.value);
        });

        var urlParams = new URLSearchParams(window.location.search);
        $('.type-selection').val(urlParams.get('type_id')).change();
        $('.breed-selection').val(urlParams.get('breed_id')).change();
        $('.sex-selection').val(urlParams.get('sex')).change();
        var max_dist = urlParams.get('max_distance');
        var max_price = urlParams.get('max_price');
        if(max_dist){
            $('.input-range-1').val(max_dist).next('.range-value').html(max_dist+'miles');
        }
        if(max_price){
            $('.input-range-2').val(max_price).next('.range-value').html(urlParams.get('max_price')+'$');
        }
    })
</script>