<!--==========================

Start : Search

============================-->

<div class="search p-b-50 p-t-60 basic-bg-color">
    <div class="container">
        <div class="row">

            <!--search area-->
            <form action="{{route('search')}}" method="GET">
                <div class="col-xs-12 col-sm-10 m-b-15">
                    <div class="search-items-wrapper">
                        <div class="col-xs-12 col-sm-3 less-padding from-group s-m-b-15">
                            <select name="type_id" class="from-control defaultSelect text-capitalize type-selection">
                                <option value="">Animal Type</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-3 less-padding from-group s-m-b-15">
                            <select name="breed_id" class="from-control defaultSelect text-capitalize breed-selection">
                                <option value="">Animal Breed</option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-3 less-padding from-group s-m-b-15">
                            <select name="sex" class="from-control defaultSelect text-capitalize sex-selection">
                                <option value="">Sex</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>

                        <div class="col-xs-12 col-sm-3 less-padding from-group s-m-b-15">
                            <button type="submit" class="search-btn">Find Your Pet</button>
                        </div>

                    </div>
                    <!-- /search-items-wrapper -->

                </div>
                <!-- /col-sm-10 -->
                {{--<div class="col-xs-12 col-sm-2 text-center">--}}

                    {{--<a href="#advanced-search" data-toggle="collapse" aria-expanded="false" aria-controls="advanced-search" role="button" class="white _16px advanced-search center-block">Advanced search <i class="fa fa-angle-down"></i></a>--}}

                {{--</div>--}}
                {{--<!-- /col-sm-2 -->--}}

                {{--<div id="advanced-search" class="collapse __web-inspector-hide-shortcut__" style="max-height: 97px;">--}}
                    {{--<div class="col-xs-12 col-sm-10">--}}
                        {{--<div class="search-items-wrapper">--}}
                            {{--<div class="col-xs-12 col-sm-3 less-padding from-group s-m-b-15">--}}
                                {{--<select name="property type" class="from-control defaultSelect text-capitalize">--}}
                                    {{--<option value="">Animal Type</option>--}}
                                    {{--<option value="">DOG</option>--}}
                                    {{--<option value="">tiger</option>--}}
                                    {{--<option value="">cat</option>--}}
                                    {{--<option value="">monkey</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-12 col-sm-3 less-padding from-group s-m-b-15">--}}
                                {{--<select name="property type" class="from-control defaultSelect text-capitalize">--}}
                                    {{--<option value="">Animal Type</option>--}}
                                    {{--<option value="">DOG</option>--}}
                                    {{--<option value="">tiger</option>--}}
                                    {{--<option value="">cat</option>--}}
                                    {{--<option value="">monkey</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}
                            {{--<div class="col-xs-12 col-sm-3 less-padding from-group s-m-b-15">--}}
                                {{--<select name="property type" class="from-control defaultSelect text-capitalize">--}}
                                    {{--<option value="">Animal Type</option>--}}
                                    {{--<option value="">DOG</option>--}}
                                    {{--<option value="">tiger</option>--}}
                                    {{--<option value="">cat</option>--}}
                                    {{--<option value="">monkey</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}

                            {{--<div class="col-xs-12 col-sm-3 less-padding from-group s-m-b-15">--}}
                                {{--<select name="property type" class="from-control defaultSelect text-capitalize">--}}
                                    {{--<option value="">Animal Type</option>--}}
                                    {{--<option value="">DOG</option>--}}
                                    {{--<option value="">tiger</option>--}}
                                    {{--<option value="">cat</option>--}}
                                    {{--<option value="">monkey</option>--}}
                                {{--</select>--}}
                            {{--</div>--}}

                        {{--</div>--}}
                        {{--<!-- /search-items-wrapper -->--}}

                    {{--</div>--}}
                    {{--<!-- /col-sm-10 -->--}}
                {{--</div>--}}
            </form>

        </div><!--/row-->
    </div>
</div>


<!--==========================
End : Search
============================-->

