<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as'=>'home', 'uses'=>'HomeController']);


//Auth related routes starts
Route::get('login',['as'=>'login', 'uses'=>'AuthController@getLogin']);
Route::post('login', 'AuthController@postLogin');
Route::post('logout', ['as'=>'logout', 'uses'=>'AuthController@logout']);
Route::post('register',['as'=>'register', 'uses'=>'AuthController@register']);
//Auth related routes ends

Route::get('test',function (){
    //return view('pet.show');
});

Route::get('search',['as'=>'search','uses'=>'SearchController']);

//Profile related routes starts
Route::group(['middleware' => 'auth'], function(){
    Route::get('me', ['as'=>'profile.show', 'uses'=>'ProfileController@show']);
    Route::get('me/edit', ['as'=>'profile.edit', 'uses'=>'ProfileController@edit']);
    Route::post('me', ['as'=>'profile.update', 'uses'=>'ProfileController@update']);
});
//Profile related routes ends

//Pet related routes starts
Route::resource('pet', 'PetController', [
    'only' => [
        'create', 'store'
    ],
    'middleware' => 'auth'
]);
Route::get('pet', ['as'=>'pet.index', 'uses'=>'PetController@index']);
Route::get('pet/{pet}', ['as'=>'pet.show', 'uses'=>'PetController@show']);
//Pet related routes ends