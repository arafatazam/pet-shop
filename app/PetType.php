<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetType extends Model
{
    public $timestamps = false;

    public function breeds(){
        return $this->hasMany('App\Breed');
    }
}
