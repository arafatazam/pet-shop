<?php

namespace App\Providers;

use App\Breed;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\SomeEvent' => [
            'App\Listeners\EventListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
        Event::listen('eloquent.saving: App\Pet',function ($pet){
            if($pet->isDirty('breed_id')){
                $pet->type_id= Breed::find($pet->breed_id)->pet_type_id;
            }
        });
    }
}
