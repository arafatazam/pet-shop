<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //@todo use required_if validation for type
            'image_file_1'=> 'required|image',
            'image_file_2'=> 'nullable|image',
            'image_file_3'=> 'nullable|image',
        ];
    }
}
