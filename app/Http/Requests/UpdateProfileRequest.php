<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Hash;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Hash::check($this->get('password'), auth()->user()->password);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'phone_number' => 'required|numeric',
            'password' => 'required',
            'address' => 'required',
            'adoptions' => 'required'
        ];
    }
}
