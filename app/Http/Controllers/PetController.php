<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePetRequest;
use App\Pet;
use App\PetType;
use Illuminate\Http\Request;

class PetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Pet $petM)
    {
        $pets = $petM->all(); //@todo paginate
        return view('pet.index',compact('pets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PetType $petTypeM
     * @return \Illuminate\Http\Response
     */
    public function create(PetType $petTypeM)
    {
        $pet_types = $petTypeM->with('breeds')->get()->toArray();
        return view('pet.create', compact('pet_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePetRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePetRequest $request)
    {
        $this->storeImages($request);
        $input = $this->getIntendedInput($request->all());
        $pet = auth()->user()->pets()->create($input);
        return redirect()->route('pet.show',[$pet->id]);
    }


    // Removes the prefix from the
    private function getIntendedInput($originalInput){
        $animal = str_slug(PetType::find($originalInput['animal_type'])->title,'_');

        $input = [];

        array_map(function($key,$value) use(&$input, $animal){
            if(strpos($key,$animal)!==false){
                $input[str_replace($animal.'_','',$key)]=$value;
            }else{
                $input[$key]=$value;
            }
        }, array_keys($originalInput),$originalInput);

        return $input;
    }


    /**
     * @param StorePetRequest $request
     */

    private function storeImages(StorePetRequest $request){
        for($i=1;$i<=3;$i++){
            if($request->hasFile("image_file_$i")){
                $path = $request->file("image_file_$i")
                                ->store(auth()->user()->id.'/pets',['disk'=>'user']);
                $request->merge(["image$i"=>$path]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pet $pet
     * @return \Illuminate\Http\Response
     */
    public function show(Pet $pet)
    {
        $pet->load('user');
        $similar = $pet->inRandomOrder()->take(8)->get();
        return view('pet.show', compact('pet','similar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pet  $pet
     * @return \Illuminate\Http\Response
     */
    public function edit(Pet $pet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pet  $pet
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pet $pet)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pet  $pet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pet $pet)
    {
        //
    }
}
