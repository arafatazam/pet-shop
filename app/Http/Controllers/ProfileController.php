<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateProfileRequest;

class ProfileController extends Controller
{

    /**
     * Display the logged in user.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $my = auth()->user()->toArray();
        return view('profile.show',compact('my'));
    }

    /**
     * Show the form for editing the logged in.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $my = auth()->user()->toArray();
        return view('profile.edit',compact('my'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProfileRequest $request
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProfileRequest $request)
    {
        auth()->user()->update($request->except('password'));
        return redirect()->route('profile.show');
    }
}
