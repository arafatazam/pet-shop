<?php

namespace App\Http\Controllers;

use App\Pet;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function __invoke(Pet $pets, Request $request)
    {

        $params = ['type_id','breed_id','sex'];

        $location='';

        foreach ($params as $param){
            if($request->has($param)){
                $pets = $pets->where($param, $request->get($param));
            }
        }

        if($request->has('coordinates')&&$request->has('max_distance')&&$request->get('max_distance')!=0){
            $location=$request->get('location_str');
            $distance_in_meters = (float)$request->get('max_distance')*1609.34;
            $from_coordinates = $request->get('coordinates');
            $pets = $pets->distance($distance_in_meters, $from_coordinates);
        }

        $pets = $pets->get();

        return view('pet.index',compact('pets','location'));
    }
}
