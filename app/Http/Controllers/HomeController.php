<?php

namespace App\Http\Controllers;

use App\Pet;

class HomeController extends Controller
{
    public function __invoke(Pet $petM){
        $latest = $petM->latest()->take(8)->get();
        return view('home.show',compact('latest'));
    }
}
