<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Storage;
use DB;

class Pet extends Model
{
    //We need mass assignment
    protected $fillable = ['name','breed_id','age','sex','color','price', 'location', 'location_string',
                            'location', 'description','image1','image2','image3'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function breed()
    {
        return $this->belongsTo('App\Breed');
    }

    public function type(){
        return $this->belongsTo('App\PetType');
    }


    // For Geospatial
    protected $geofields = array('location');


    public function setLocationAttribute($value) {
        $this->attributes['location'] = DB::raw("POINT($value)");
    }

    public function getLocationAttribute($value){

        $loc =  substr($value, 6);
        $loc = preg_replace('/[ ,]+/', ',', $loc, 1);

        return substr($loc,0,-1);
    }

    public function newQuery($excludeDeleted = true)
    {
        $raw='';
        foreach($this->geofields as $column){
            $raw .= ' astext('.$column.') as '.$column.' ';
        }

        return parent::newQuery($excludeDeleted)->addSelect('*',DB::raw($raw));
    }

    /**
     * @param $query
     * @param $dist_in_meter
     * @param $location
     * @return mixed
     */
    public function scopeDistance($query, $dist_in_meter, $location)
    {
        return $query->whereRaw('st_distance_sphere(location,POINT('.$location.')) < '.$dist_in_meter);
    }

    // For Geospatial Ends

    public function getImage1Attribute($path){
        if($path){
            return Storage::disk('user')->url($path);
        }
    }

    public function getImage2Attribute($path){
        if($path){
            return Storage::disk('user')->url($path);
        }
    }

    public function getImage3Attribute($path){
        if($path){
            return Storage::disk('user')->url($path);
        }
    }
}
