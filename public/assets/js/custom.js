$( document ).ready(function() {

  "use strict";





  /*-------------------------------------
  add niceScroll
  -------------------------------------*/
  $("html").niceScroll({
      mousescrollstep:30,
      cursorcolor: "#FF9935",
      zindex: 9999,
      cursorborder: "none",
      cursorwidth: "4px",
      cursorborderradius: "none"
  });


 




/*---------------------------------
 Add wow.js
 --------------------------------*/
 var wow = new WOW(
  {
    mobile: false
  }
);
wow.init();


$('select').click(function(){
    $(this).toggleClass('colorBlack');;
});




/*range slider*/
$('.input-range-1').on('input', function() {
  $(this).next('.range-value').html(this.value+'miles');
});

$('.input-range-2').on('input', function() {
  $(this).next('.range-value').html(this.value +'$');
});





 /*-------------------------
  add stellar.js
  -------------------------*/
  $(window).stellar();





/*-----------------------------------
 ADD Owl carousel for customer section
 ----------------------------------*/
 
 $('.wrapper-slide').owlCarousel({
        items:1,
        margin:10,
        loop:true,
        nav:true,
        navText : ["<i class='fa fa-angle-left btn'></i>","<i class='fa fa-angle-right btn'></i>"]
  });


/*-----------------------------
 Latest product slider
-----------------------------*/

$(".common-btn").on("click",function(){
  var getClassName = $(this).attr("data-number");
  var makeitNum = Number(getClassName);
  

  if(makeitNum===0){
    $('.belt').css("transform" , "translateX(0)");
  }

  if(makeitNum===1){
    $('.belt').css("transform" , "translateX(-20%)");
  }

  if(makeitNum===2){
    $('.belt').css("transform" , "translateX(-40%)");
  }

  if(makeitNum===3){
    $('.belt').css("transform" , "translateX(-60%)");
  }

  if(makeitNum===4){
    $('.belt').css("transform" , "translateX(-80%)");
  }
});

//login form animation
$('.login-container-wrapper #go-next').click(function(){
  $(this).parents('.login-container-wrapper').toggleClass('is-transitioned');
});

//chicken slider
$('.thumbnail').on('click',function(){
    var data = $(this).attr("data-index");
    var dataNumber = data * 1;
    var width = 10;

     $('.slider-wrapper').css({
      'transform':'translateX('+'-'+ width*dataNumber+'%)'
     });
});

var prev = $('#prev');
var next = $('#next');

var lenght = $('.slide').length;
var width = 10;
var i = 0;


next.on('click',function(){
  i++;
  if (i>lenght-1) {
    i = 0;
  }

  $('.slider-wrapper').css({
    'transform':'translateX(' + '-' + width*i +'%)'
  });
  
});

prev.on('click',function() {
  i--;
  if (i<0) {
    i = lenght-1;
  }

  $('.slider-wrapper').css({
    'transform':'translateX(' + '-' + width*i +'%)'
  });
  
});




/*-----------------------------------
 Select the type (five.html)
 ----------------------------------*/

$( "select#animal-select" )
  .change(function() {

    $( "select#animal-select option:selected" )
    .not(':first-child')

    .each(function() {

      var data = $(this).attr('data-target');

      $(".same-all[data-index="+ data +"]")
      .show(400)
      .siblings('.same-all')
      .hide(300);
    });


    


  })
  .trigger( "change" );

  $('.same-all').hide();




 






   


  /*----------------------------------------------------------------------
   Bootstrap Internet Explorer 10 in Windows 8 and Windows Phone 8 FIX
  -----------------------------------------------------------------------*/
  if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement('style');
    msViewportStyle.appendChild(
      document.createTextNode(
        '@-ms-viewport{width:auto!important}'
      )
    );
    document.querySelector('head').appendChild(msViewportStyle);
  }

/*----------------------------------------------------------------------
end: Bootstrap Internet Explorer 10 in Windows 8 and Windows Phone 8 FIX
-----------------------------------------------------------------------*/ 


});
 







